proc getCodeNumber {code} {

    set paises(500) {Falkland Islands}
    set paises(500) {South Georgia and the South Sandwich Islands}
    set paises(501) Belize
    set paises(502) Guatemala
    set paises(503) {El Salvador}
    set paises(504) Honduras
    set paises(505) Nicaragua
    set paises(506) {Costa Rica}
    set paises(507) Panama
    set paises(508) {Saint-Pierre and Miquelon}
    set paises(509) Haiti
    set paises(51) Peru
    set paises(52) Mexico
    set paises(53) Cuba
    set paises(54) Argentina
    set paises(55) Brazil
    set paises(56) Chile
    set paises(57) Colombia
    set paises(58) Venezuela
    set paises(590) {Guadeloupe (including Saint Barthélemy, Saint Martin)}
    set paises(591) Bolivia
    set paises(592) Guyana
    set paises(593) Ecuador
    set paises(594) {French Guiana}
    set paises(595) Paraguay
    set paises(596) Martinique
    set paises(597) Suriname
    set paises(598) Uruguay
    foreach datos [array names paises] {
        if {[string equal $datos $code]} {
            return $paises($code)
        } 
    }
    return {Codigo no encontrado}

}
puts [getCodeNumber 502]
proc OrderBy {{order "ASC"}} {
    set datos [read [open "textPrueba.txt" r+]]
    set order [string toupper $order]
    switch $order {
        "ASC" {
            return  [lsort -dictionary  $datos]
        }
        "DESC" {
            return  [lsort -dictionary -decreasing $datos]
        }
    }
}

puts [OrderBy desc1]


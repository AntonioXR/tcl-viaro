proc datos {} {
    set datos [glob -types f *]
    puts "\nArchivos:"
    foreach var $datos {
        puts $var
        for {set i [string length $var]} { $i > 0 } {set i [expr $i - 1]} {
            if {[string equal [string range $var $i $i] "."]} {
                set ext [string range $var [expr $i + 1] [string length $var]]
                if {[string match *$ext* [array names extenciones]]} {
                    set extenciones($ext) [expr $extenciones($ext) + 1]
                } else {
                    set extenciones($ext) 1
                }
            }
        }
    }
    puts "\nTotales:"   
    foreach var [array names extenciones] {
        set dataGet [eval [list exec] [auto_execok dir] [list "*.$var"]]
        set totalBytes [lindex $dataGet [expr [llength $dataGet] - 7]]
        puts "Extencion $var: $extenciones($var) Archivo(s) Total: $totalBytes Bytes"
    }
    set dataGet [eval [list exec] [auto_execok dir]]
    set totalBytes [lindex $dataGet [expr [llength $dataGet] - 7]]
    puts "\nTamanio total de archivos: $totalBytes Bytes"
}
#puts [datos]

puts [eval [list exec] [auto_execok dir]]

proc getX {number} {
	set Xcount {}
	for {set i 0} {$i < $number} {incr i} {
		append Xcount {*}
	}
	return $Xcount
}

proc  censurar {args} {
	set marcas {Oracle Pepsi Ferrari Viaro}
	foreach var $marcas {
		set args [string map -nocase [list $var [getX [string length $var]]] $args]
	}
	return $args
}

puts [censurar "Me gusta tomar Pepsi para mantenermeViarodespierto mientras manejo mi Ferrari"]
proc vector2 {vector1 vector2} {
    #Suma de vectores
    set resultado_suma "X: [expr [lindex $vector1 0] + [lindex $vector2 0]] | Y: [expr [lindex $vector1 1] + [lindex $vector2 1]]"

    #Producto Punto
    set resultado_1 "[expr [lindex $vector1 0] * [lindex $vector2 0]] [expr [lindex $vector1 1] * [lindex $vector2 1]]" 
    set resultado_producto "Producto Punto: [expr [lindex $resultado_1 0] + [lindex $resultado_1 1]]"

    return [list $resultado_suma $resultado_producto]
}

proc vector3 {vector1 vector2} {

     #Suma de vectores
    set resultado_suma "X: [expr [lindex $vector1 0] + [lindex $vector2 0]] | Y: [expr [lindex $vector1 1] + [lindex $vector2 1]] | Z: [expr [lindex $vector1 2] + [lindex $vector2 2]]"
    
    #Producto Punto
    set resultado_1 "[expr [lindex $vector1 0] * [lindex $vector2 0]] [expr [lindex $vector1 1] * [lindex $vector2 1]] [expr [lindex $vector1 2] * [lindex $vector2 2]]" 
    set resultado_producto "Producto Punto: [expr [lindex $resultado_1 0] + [lindex $resultado_1 1] + [lindex $resultado_1 2]]"
    
    return [list $resultado_suma $resultado_producto]

}

proc vectores {vector1 vector2} {
 if {[expr [llength $vector1] == [llength $vector2]]} { 
     switch [llength $vector1] {
         "2" {
             return [vector2 $vector1 $vector2]
         }
         "3" {
             return [vector3 $vector1 $vector2]
         }
         default {
             return {El vector no puede se de mas de 3 elementos ni menos de 2}
         }
     }
 } else {
     return {No tienen la misma cantidad de datos}
 }
}

set datos [vectores {1 2} {2 1}]

puts [lindex $datos 0]
puts [lindex $datos 1]
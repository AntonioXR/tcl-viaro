proc barra {datos} {
    for {set i [string length $datos]} { $i > 0 } { set i [expr $i - 1] } {
        if {[string equal [string range $datos $i $i] "/"]} {
            return [string range $datos [expr $i + 1] [string length $datos]]
            break
        }
    }
    return ""
}

proc allDir {{_pwd 0}} {
    set datos [pwd]
    set retorno ""
    if {$_pwd == 0} {
        set lista [glob -nocomplain -dir $datos -type d "*"]
        foreach var $lista {
            set obtenido [allDir $var]
            set var [barra $var]
            append var "\{ $obtenido \}"
            lappend retorno $var
        }
    } else {
        set lista [glob -nocomplain -dir $_pwd -type d *]
            foreach var $lista {
                set obtenido [allDir $var]
                set var [barra $var]
                append var "\{ $obtenido \}"
                lappend retorno $var 
            }
    }
    return $retorno
}

set lista [allDir]
puts "Carpeta 2: [lindex $lista 1]"
puts $lista

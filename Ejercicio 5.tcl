proc stringRandom {largo} {
    set letras {a b c d e f g h i j k l m n o p q r s t u v w x y z}
    set cadena {}
    for { set i 0} {$i < $largo} {incr i} {
        set numeroR [expr int([expr rand() * 30])]
        if {$numeroR <= 0} {
            set numeroR [expr $numeroR + 2 * 3]
        } elseif {$numeroR > 26} {
            set numeroR [expr $numeroR / 5 +5 ]
        }
        append cadena [lindex $letras $numeroR]
    }
    return $cadena
}

puts [stringRandom 5]

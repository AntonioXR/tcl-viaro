proc factorial {numero} {
    for {set i [expr $numero - 1]} {$i > 1} {set i [expr $i - 1]} {
        set numero [expr $numero * $i]
    }
    return [expr $numero == 0? 1 : $numero]
}
 puts [factorial 5]
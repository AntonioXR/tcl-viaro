proc datos {} {
    set datos [glob -types f *]
    puts "\nArchivos:"
    foreach var $datos {
        puts $var
        for {set i [string length $var]} { $i > 0 } {set i [expr $i - 1]} {
            if {[string equal [string range $var $i $i] "."]} {
                set ext [string range $var [expr $i + 1] [string length $var]]
                if {[string match *$ext* [array names extenciones]]} {
                    set extenciones($ext) [expr $extenciones($ext) + 1]
                } else {
                    set extenciones($ext) 0
                    set extenciones($ext) [expr $extenciones($ext) + 1]
                }
            }
        }
    }
    puts "\nTotales:"
    foreach var [array names extenciones] {
        puts "Extencion $var: $extenciones($var) Archivo(s)"
    }
}

datos
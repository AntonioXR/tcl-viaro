
proc factorial {numero} {
    for {set i [expr $numero - 1]} {$i > 1} {set i [expr $i - 1]} {
        set numero [expr $numero * $i]
    }
    return $numero
}

proc factoriales {args} {
	set resultado {}
	set contador 0
	set ultimo 0

	for {set i 0} {$i < [expr [string length $args]]} {incr i} {

		if {[string equal {,} [string range $args $contador $i]] } {
			set datos [string range $args $ultimo [expr $contador - 1]]
			set obtenido [factorial $datos]
			append resultado $obtenido ";"
			set ultimo [expr $contador + 1]
		}

		if {[expr [expr [string length $args] - 1] == $i]} {
			set datos [string range $args $ultimo $i]
			set obtenido [factorial $datos]
			append resultado $obtenido
		}

		set contador [expr $contador + 1]
	}
	return $resultado
}

puts "Para 1,4,3"
puts "[factoriales {1,4,3}] \n"
puts "Para 10,3"
puts "[factoriales {10,3}] \n"
puts "Para 3,15"
puts "[factoriales {3,15}] \n"